## Anggota Kelompok

Mohammad Fadhil Rasyidin Parinduri // 5025201131

Farrel Emerson // 5025201082

Samuel // 5025201187

### Soal Shift
- [link](https://docs.google.com/document/d/13HAYs1Dp9-gZ0RtFUQqf-SULoj4dL9JG0LoiKgrOY7Y/edit?usp=sharing)

# Soal 1
Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan ketentuan berikut:  
## 1a. 
Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13  
Contoh :  
```
“Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”
```
## Penyelesaian
Untuk mengklasifikasikan apakah suatu direktori akan diencode atau didecode, kami membuat fungsi isAnimeku yang mengembalikan nilai true atau false. Ketika isAnimeku bernilai true, yaitu ketika terdapat substring "Animeku_", maka direktori path yang dimasukkan akan diencode, sebaliknya ketika bernilai false maka akan di-decode.
Fungsi isAnimeku:
```
bool isAnimeku(const char *path) 
{
    for(int i=0;i<strlen(path)-8+1;i++)
        if(path[i] == 'A' && path[i+1] == 'n' && path[i+2] == 'i' && path[i+3] == 'm' && path[i+4] == 'e'
        && path[i+5] == 'k' && path[i+6] == 'u' && path[i+7] == '_') return 1;
    return 0;
}
```
Fungsi pengencode-an atbash+rot13:
```
void encodeAtRot(char *s)
{
    for(int i=0;i<strlen(s);i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+13)%26)+'a';
}

void decodeAtRot(char *s)
{
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = 'Z'-s[i]+'A';
        else if(s[i]>='a'&&s[i]<110) s[i] = ((s[i]-'a'-13)+26)+'a';
        else if(s[i]>=110&&s[i]<='z') s[i] = ((s[i]-'a'-13)%26)+'a';
}
```
## 1b.
Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.
## Penyelesaian
Fungsi fuse untuk xmp_rename akan dipanggil ketika terdapat suatu direktori atau file yang direname. Pada fungsi ini, kita bisa mengecek apakah direktori diubah menjadi direktori yang akan di-encode atau di-decode.  
Oleh karena itu, pada fungsi xmp_rename, kami mendeklarasikan string fpath (nama direktori sebelumnya) dan tpath (nama direktori sesudahnya).  
`fpath ` atau `tpath` akan di-encode ketika memiliki substring "Animeku_". Jika tidak, direktori tersebut akan di-decode.  
Pada fungsi fuse xmp_rename:
```
        if (isAnimeku(fpath) && !isAnimeku(tpath)) 
        {
            printf("[Mendekode %s.]\n", fpath);
            sistemLog(fpath, tpath, 2);
            int itung = decodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terdekode: %d]\n", itung);
        }
        else if (!isAnimeku(fpath) && isAnimeku(tpath)) 
        {
            printf("[Mengenkode %s.]\n", fpath);
            sistemLog(fpath, tpath, 1);
            int itung = encodeFolderRekursif(fpath, 1000);
            printf("[Total file yang terenkode: %d]\n", itung);
```

## 1c. 
Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.  
## Penyelesaian
Sama dengan penjelasan pada nomor 1b
## 1d. 
Setiap data yang terencode akan masuk dalam file “Wibu.log”  
Contoh isi:  
```
RENAME terenkripsi /home/ricky/Downloads/hai --> /home/ricky/Downloads/Animeku_hebat  
RENAME terdecode /home/ricky/Downloads/Animeku_ --> /home/ricky/Downloads/Coba
```  
## Penyelesaian
Untuk menyelesaikan problem ini, kami membuat fungsi `logRename` yang terintegrasi dengan fungsi `sistemLog` yang dapat mencatat aktivitas rename dari suatu direktori. Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi.  
Fungsi `sistemLog`:
```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    }    
}
```
dan fungsi `logRename`:
```
void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}
```
  
## 1e. 
Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya. (rekursif)  
```
Note : filesystem berfungsi normal layaknya linux pada umumnya, Mount source (root)
filesystem adalah directory /home/[USER]/Documents, dalam penamaan file ‘/’
diabaikan, dan ekstensi tidak perlu di-encode
```  
## Penyelesaian
Untuk menyelesaikan problem ini, kita akan menggunakan fungsi-fungsi dari library dirent.h. Kemudian, kita akan mengimplementasikan library dirent.h di fungsi `decodeFolderRekursif` maupun fungsi `encodeFolderRekursif` dengan nilai yang dikembalikan adalah total file yang berhasil di-encode ataupun di-decode.  
Pada fungsi `encodeFolderRekursif` dan `decodeFolderRekursif` akan dilakukan proses scan file maupun folder di dalamnya. Proses decode maupun encode akan dilanjutkan pada suatu folder yang berhasil ditemukan berdasarkan parameter `basePath` yang dimasukkan saat pemanggilan fungsi.  
Fungsi `encodeFolderRekursif`:
```
int encodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000]; 
    struct dirent *dp; 
    DIR *dir = opendir(basePath);
    if (!dir) return 0;
    int itung=0;
    while((dp=readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode)&&depth>0)
            itung += encodeFolderRekursif(path, depth - 1),
            encodeFolder(basePath, dp->d_name);
        else if(encodeFile(basePath, dp->d_name) == 0) itung++;
    }
    closedir(dir);
    return itung;
}
```
Fungsi `decodeFolderRekursif`:
```
int decodeFolderRekursif(char *basePath, int depth) 
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    if(!dir) return 0;
    int itung = 0;
    while((dp = readdir(dir)) != NULL) 
    {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;
        strcpy(path, basePath); strcat(path, "/"); strcat(path, dp->d_name);
        struct stat path_stat;
        stat(path, &path_stat);
        if(!S_ISREG(path_stat.st_mode) && depth>0)
            itung += decodeFolderRekursif(path, depth - 1),
            decodeFolder(basePath, dp->d_name);
        else if(decodeFile(basePath, dp->d_name) == 0) itung++;
    }
    closedir(dir);
    return itung;
}
```

# Soal 2
Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan ketentuan sebagai berikut :

## 2a
Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

## Penyelesaian 
Untuk mengklasifikasikan apakah suatu direktori akan diencode atau didecode, kami membuat fungsi isAnimeku yang mengembalikan nilai true atau false. Ketika isAnimeku bernilai true, yaitu ketika terdapat substring "IAN_", maka direktori path yang dimasukkan akan diencode, sebaliknya ketika bernilai false maka akan di-decode.
Fungsi isIAN:
```
bool isIAN(const char *path) 
{
    for(int i=0;i<strlen(path)-4+1;i++)
        if(path[i] == 'I' && path[i+1] == 'A' && path[i+2] == 'N' && path[i+3] == '_') return 1;
    return 0;
}
```
Fungsi Encode dan Decode  Vigenere Cipher dengan key “INNUGANTENG” (case sensitive)
```
void encodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for (int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'+(key[i%((sizeof(key)-1))]-'A'))%26)+'A';
        else if('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'+(key[i%((sizeof(key)-1))]-'A'))%26)+'a';
}

void decodeVig(char *s) 
{
    char key[] = "INNUGANTENG";
    for(int i=0;s[i];i++)
        if('A' <= s[i] && s[i] <= 'Z') s[i] = ((s[i]-'A'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'A';
        else if ('a' <= s[i] && s[i] <= 'z') s[i] = ((s[i]-'a'-(key[i%((sizeof(key)-1))]-'A')+26)%26)+'a';
}
```

## 2b
Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

## Penyelesaian
Fungsi fuse untuk xmp_rename akan dipanggil ketika terdapat suatu direktori atau file yang direname. Pada fungsi ini, kita bisa mengecek apakah direktori diubah menjadi direktori yang akan di-encode atau di-decode.
Oleh karena itu, pada fungsi xmp_rename, kami mendeklarasikan string fpath (nama direktori sebelumnya) dan tpath (nama direktori sesudahnya).
fpath  atau tpath akan di-encode ketika memiliki substring "Animeku_". Jika tidak, direktori tersebut akan di-decode.
Pada fungsi fuse xmp_rename:
```
else if(isIAN(fpath) && !isIAN(tpath)){
        printf("[Mendekode %s.]\n", fpath);
        sistemLog(fpath, tpath, 2);
        int itung = decodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terdekode: %d]\n", itung);
}else if(!isIAN(fpath) && isIAN(tpath)){
        printf("[Mengenkode %s.]\n", fpath);
        sistemLog(fpath, tpath, 1);
        int itung = encodeFolderRekursifIAN(fpath, 1000);
        printf("[Total file yang terenkode: %d]\n", itung);
}
```

## 2c
Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

## Penyelesaian
Sama seperti pada nomor 2b

## 2d dan 2e
Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

## Penyelesaian
Untuk menyelesaikan problem ini, kami membuat fungsi `logInfo` dan logWarning yang terintegrasi dengan fungsi `sistemLog` yang dapat mencatat aktivitas rename dari suatu direktori. Fungsi ini nantinya akan dipanggil sebelum proses encode maupun decode dieksekusi. Fungsi `sistemLog` menerima parameter `tipe`, dimana variable ini akan menentukan kategori dari sebuah system call. Di mana ketika tipe 3, maka termasuk INFO, dan ketika tipe 4 dan 5 akan termasuk warning dengan command masing-masing (RMDIR atau UNLINK).

```
void sistemLog(char *dir1, char *dir2, int tipe) 
{
    char buff[1024], cmd[32];
    if(dir1[0]!='\0') strcpy(cmd, "RENAME"), sprintf(buff, "%s --> %s", dir1, dir2), logRename(cmd, tipe, buff), logIngfo(cmd,buff);
    else{
        if(tipe == 3){ //mkdir
            strcpy(cmd, "MKDIR"), sprintf(buff, "%s", dir2), logIngfo(cmd, buff);
        }else if(tipe == 4){ //rmdir
            strcpy(cmd, "RMDIR"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }else if(tipe == 5){ //unlink
            strcpy(cmd, "UNLINK"), sprintf(buff, "%s", dir2), logWarning(cmd, buff);
        }
    } 
    
}
```
Fungsi `logIngfo`
```
void logRename(char *cmd, int tipe, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t);
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt);
    char logNya[1100];
    sprintf(logNya, "%s %s %s", cmd, tipe==1?"terenkripsi":"terdecode", des);
    FILE *out = fopen(fileLog, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
    return;
}
```
Fungsi `logWarning`
```
void logWarning(char *cmd, char *des) 
{
    time_t t = time(NULL);
    struct tm* lt = localtime(&t); 
    char waktu[30];
    strftime(waktu, 30, "%d%m%Y-%H:%M:%S", lt); 
    char logNya[1100];
    sprintf(logNya, "WARNING::%s:%s::%s", waktu, cmd, des); 
    FILE *out = fopen(fileLogHayo, "a");
    fprintf(out, "%s\n", logNya);
    fclose(out);
}
```


## 3a. 
Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial.

```
static int xmp_mkdir(const char *path, mode_t mode) 
{
    int res; char fpath[1000];
    if (strcmp(path, "/") == 0) path = direktori, sprintf(fpath, "%s", path);
    else sprintf(fpath, "%s%s", direktori, path);
    res = mkdir(fpath, mode);
    if (res == -1) return -errno;
    if (isAnimeku(fpath)) sistemLog("", fpath, 3);
    else if(isIAN(fpath)) sistemLog("", fpath, 3);
    else if (isNamdosaq(fpath)) encryptSpecial(fpath);sistemLog("", fpath, 3);
    return 0;
}
```
## 3b, 3d, 3e
Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial. Pada fungsi rename kita akan mengecek jika direktori dirubah dengan awalan nam_do-saq kemudian mengembalikan enkripsi/encoding pada direktori.

```
// rename Namdosaq to Animeku
else if (isNamdosaq(fpath) && isAnimeku(tpath))
{
    encryptSpecial(fpath);
}
// rename Namdosaq to IAN
else if (isNamdosaq(fpath) && isIAN(tpath))
{
    encryptSpecial(fpath);
}

// rename Animeku to Namdosaq
else if (isAnimeku(fpath) && isNamdosaq(tpath))
{
    decryptSpecial(fpath);
    sistemLog(fpath, tpath, 2);
}

// rename IAN to Namdosaq
else if (isIAN(fpath) && isNamdosaq(tpath))
{
    decryptSpecial(fpath);
    sistemLog(fpath, tpath, 2);
}
```

kemudian pada enkripsi juga dilakukan perubahan semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.
Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan
menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

```
void getBiner(char *fname, char *bin, char *uppercase){
	int idFirst = 0;
    for(int i=0; i<strlen(fname); i++){
		if (fname[i] == '/') {
            idFirst = i + 1;
        }
	}
    int idEnd = strlen(fname);
    for(int i=strlen(fname)-1; i>=0; i--){
		if (fname[i] == '.') {
            idEnd = i;
        }
	}

	int i;
	for(i=idFirst; i<idEnd; i++){
		if(isupper(fname[i])){
			bin[i] = '0';
            uppercase[i] = fname[i];
		}
		else{
			bin[i] = '1';
            uppercase[i] = fname[i] - 32;
		}
	}
	bin[idEnd] = '\0';
	
	for(; i<strlen(fname); i++){
		uppercase[i] = fname[i];
	}
	uppercase[i] = '\0';
}

int binerToDecimal(char *bin){
	int temp = 1, result = 0;
	for(int i=strlen(bin)-1; i>=0; i--){
        if(bin[i] == '1') {
            result += temp; 
            temp *= 2;
        }
    }
	return result;
}

void encryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat path_stat;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathBinary[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &path_stat) < 0);
		else if (S_ISDIR(path_stat.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0){
                continue;
                sprintf(dirPath,"%s/%s",filepath, dp->d_name);
                encryptSpecial(dirPath);
            }
		}
        else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char bin[1000], uppercase[1000]; 
            getBiner(dp->d_name, bin, uppercase);
			int dec = binerToDecimal(bin);
			sprintf(filePathBinary,"%s/%s.%d",filepath,uppercase,dec); 
            rename(filePath, filePathBinary);
		}
	}
    closedir(dir);
}
```

## 3c.
Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

```
int convertToDecimal(char *ext){
	int dec = 0, temp = 1;
	for(int i=strlen(ext)-1; i>=0; i--){
        dec += (ext[i]-'0')*temp;
        temp *= 10;
    }
	return dec;
}

void decimalToBiner(int dec, char *bin, int len){
	int idx = 0;
	while(dec){
		if(dec & 1){
            bin[idx] = '1';
        }
		else {
            bin[idx] = '0';
            idx++;
        }
		dec /= 2;
	}
	while(idx < len){
		bin[idx] = '0'; 
        idx++;
	}
	bin[idx] = '\0';
	
	for(int i=0; i<idx/2; i++){
		char temp = bin[i];
        bin[i] = bin[idx-1-i];
        bin[idx-1-i] = temp;
	}
}

void getDecimal(char *fname, char *bin, char *normalcase){
	int idFirst = 0;
    for(int i=0; i<strlen(fname); i++){
		if (fname[i] == '/') {
            idFirst = i + 1;
        }
	}
    int idEnd = strlen(fname);
    for(int i=strlen(fname)-1; i>=0; i--){
		if (fname[i] == '.') {
            idEnd = i;
        }
	}
	int i;
	
	for(i=idFirst; i<idEnd; i++){
		if(bin[i-idFirst] == '0') normalcase[i-idFirst] = fname[i] + 32;
		else normalcase[i-idFirst] = fname[i];
	}
	
	for(; i<strlen(fname); i++){
		normalcase[i-idFirst] = fname[i];
	}
    normalcase[i-idFirst] = '\0';
}

void decryptSpecial(char *filepath){
	chdir(filepath);
	DIR *dir = opendir(".");
	struct dirent *dp;
	struct stat path_stat;
	if(dir == NULL) return;
	
	char dirPath[1000];
	char filePath[1000];
	char filePathDecimal[1000];
	
    while ((dp = readdir(dir)) != NULL){
		if (stat(dp->d_name, &path_stat) < 0);
		else if (S_ISDIR(path_stat.st_mode)){
			if (strcmp(dp->d_name,".") == 0 || strcmp(dp->d_name,"..") == 0) {
                continue;
                sprintf(dirPath,"%s/%s",filepath, dp->d_name);
                decryptSpecial(dirPath);
            }
		}
		else{
			sprintf(filePath,"%s/%s",filepath, dp->d_name);
			char fname[1000], bin[1000], normalcase[1000], clearPath[1000];
			
			strcpy(fname, dp->d_name);
			char *ext = strrchr(fname, '.');
			int dec = convertToDecimal(ext+1);
			for(int i=0; i<strlen(fname)-strlen(ext); i++) {
                clearPath[i] = fname[i];
            }
			
			char *ext2 = strrchr(clearPath, '.');
			decimalToBiner(dec, bin, strlen(clearPath)-strlen(ext2));
            getDecimal(clearPath, bin, normalcase);
            sprintf(filePathDecimal,"%s/%s",filepath,normalcase);
            rename(filePath, filePathDecimal);
		}
	}
    closedir(dir);
}
```

Kendala:
1. Secara pengetahuan masih kurang mumpuni
2. Sulit memahami materi, maupun mencari sumber materi dari internet
3. Pembagian waktu pengerjaan yang kurang bagus/tepat (terutama karena liburan)
